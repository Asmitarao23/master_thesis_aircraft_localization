# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 15:36:13 2020

@author: Asmita
"""

import pdb
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.multioutput import MultiOutputRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error
from matplotlib import pyplot as plt
from variation_calc import calc_variation
from sklearn.model_selection import KFold
from time_based_split import time_based_split
from distance_based_split import distance_based_split
import folium
from sklearn import metrics
import datetime

dataframe = pd.read_csv("training_1_category_1.csv")

# using time

df = time_based_split(dataframe, 4, test_size=0.3)
df = df.dropna(axis=0, how='any')

X = df.iloc[:, 2:6].values
y = df.iloc[:, 0:2].values



model = XGBRegressor(n_estimators=1000)


cv = KFold(n_splits=10)

for train_index, test_index in cv.split(X):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # For training, fit() is used
    multioutputregressor = MultiOutputRegressor(model).fit(X_train, y_train)

    # Default metric is R2 for regression, which can be accessed by score()
    multioutputregressor.score(X_test, y_test)

    # For other metrics, we need the predictions of the model
    y_pred = multioutputregressor.predict(X_test)

    print(metrics.mean_squared_error(y_test, y_pred))
    print(metrics.r2_score(y_test, y_pred))


print('Mean Absolute Error:', multioutputregressor.score(X_test, y_test))
print('Mean Absolute Error:', mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(
    mean_squared_error(y_test, y_pred)))
print('Average distance error: ', calc_variation(y_test, y_pred))


dataframe_1787 = pd.read_csv("plane_1983.csv")    
df_1787 = time_based_split(dataframe_1787, num_sensors= 4, test_size=0.3)
df_1787 = df_1787.dropna(axis=0, how='any')
X_1787 = df_1787.iloc[:, 2:6].values
y_1787 = df_1787.iloc[:, 0:2].values

y_pred_1787 = multioutputregressor.predict(X_1787)
print('Average distance error: ', calc_variation(y_1787, y_pred_1787))

'''
m = folium.Map(location=[49.5227361129502, 7.80571190086571])
folium.PolyLine(y_1787, color="red", weight=2.5, opacity=1).add_to(m)
[folium.CircleMarker(row, radius=1, popup="", color='green', fill_color="red").add_to(m) for row in y_pred_1787]
m.save("svr_plot_2.html")

'''
