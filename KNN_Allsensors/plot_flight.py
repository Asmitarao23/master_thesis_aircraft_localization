import pdb
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

df = pd.read_csv('training_1_category_1.csv')
flights = df[[
    "id", "timeAtServer", "aircraft", "latitude", "longitude", "baroAltitude", "geoAltitude", "numMeasurements", "measurements"]].to_numpy()


latlong = []
for flight in flights:
    if flight[2] == 1787:
        coord = [flight[3], flight[4]]
        latlong.append(coord)


# for coord in latlong:
#     plt.scatter(coord[0], coord[1])


data = np.array(latlong)
x, y = data.T
plt.scatter(x, y)
plt.savefig("mygraph.png")
