def train_test_split(data, perc):
    data = data.values()
    n = int(len(data) * (1 - perc))
    return data[:n], data[n:]
