# from geopy.distance import geodesic
from math import sin, cos, sqrt, atan2, radians


def calc_variation(train,test,plane=False):
    distance = []
    for i in range(len(train)):
        R = 6373.0
        lat1 = radians(train[i][0])
        lon1 = radians(train[i][1])
        lat2 = radians(test[i][0])
        lon2 = radians(test[i][1])

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance.append(R * c)
             
        average = sum(distance) / len(distance)
    
    if plane:
        distance.sort()
        import pdb;pdb.set_trace()
        intersection_datasets = pd.DataFrame({"distance_diff": distance[:]})
        intersection_datasets.to_csv('knn_diff_all.csv', encoding='utf-8')
#%%    
    return round(average, 2)

#%%