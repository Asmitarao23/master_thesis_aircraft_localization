# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 01:22:50 2020

@author: Asmita
"""

import pdb
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error
from matplotlib import pyplot as plt
from variation_calc import calc_variation
from time_based_split import time_based_split
from distance_based_split import distance_based_split
import folium
from sklearn import metrics
import datetime
from get_data import get_data

#dataframe = pd.read_csv("training_1_category_1.csv")
dataframe = pd.read_csv("SampleFile.csv")
# all sensors
df = time_based_split(dataframe, num_sensors= None, test_size=0.3)
df = df.dropna(axis=0, how='any')
X = df.iloc[:, 2:].values
y = df.iloc[:, 0:2].values

'''

x   y    s1   s2   s3   s400     ..... s536
4,  7    0    0    0     1

'''


model = RandomForestRegressor(n_estimators=100, random_state=0)

cv = KFold(n_splits=10)

for train_index, test_index in cv.split(X):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # For training, fit() is used
    model.fit(X_train, y_train)

    # Default metric is R2 for regression, which can be accessed by score()
    model.score(X_test, y_test)

    # For other metrics, we need the predictions of the model
    y_pred = model.predict(X_test)

    print(metrics.mean_squared_error(y_test, y_pred))
    print(metrics.r2_score(y_test, y_pred))



print('Mean Absolute Error:', mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(
    mean_squared_error(y_test, y_pred)))

print('Average distance error: ', calc_variation(y_test, y_pred))


dataframe_1787 = pd.read_csv("plane_1983.csv")  
plane_1983 = get_data(dataframe)
plane_1983 = plane_1983.dropna(axis=0, how='any')
X_1787 = plane_1983.iloc[:, 2:].values
y_1787 = plane_1983.iloc[:, 0:2].values

y_pred_1787 = model.predict(X_1787)
print('Average distance error: ', calc_variation(y_1787, y_pred_1787))

#For generating the .csv file with difference in Kms for each of the predictions
'''
 temp = pd.DataFrame({"distance_diff":distance[:600]})
 temp.to_csv('SVR_TimeBased.csv', encoding='utf-8')
''' 
'''
m = folium.Map(location=[49.5227361129502, 7.80571190086571])
folium.PolyLine(y_1787, color="red", weight=2.5, opacity=1).add_to(m)
[folium.CircleMarker(row, radius=1, popup="", color='green', fill_color="red").add_to(m) for row in y_pred_1787]
m.save("svr_plot_2.html")

'''
