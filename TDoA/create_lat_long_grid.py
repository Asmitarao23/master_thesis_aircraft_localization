import os

import numpy as np
import pandas as pd

latitude = [28, 68]
longitude = [-18, 39]
step_range = 0.1
intersection_coords = []

for long_intersection in np.arange(longitude[0], longitude[1]+step_range, step_range):
    for lat_intersection in np.arange(latitude[0], latitude[1]+step_range, step_range):
        intersection_coords.append(
            [round(lat_intersection, 1), round(long_intersection, 1)])


np_intersections_coords = np.array(intersection_coords)


intersection_datasets = pd.DataFrame(
    {"lat": np_intersections_coords[:, 0], "long": np_intersections_coords[:, 1]})

file_name = 'intersections_min.csv'
file_path = os.path.join(os.getcwd(), file_name)
if os.path.exists(file_path):
    os.remove(file_path)


intersection_datasets.to_csv('intersections_min.csv', encoding='utf-8')