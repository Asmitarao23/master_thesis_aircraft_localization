import pdb
import ast
import os

import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from matplotlib import pyplot as plt

from geopy.distance import geodesic


sensors_df = pd.read_csv('sensors.csv')
intersections_df = pd.read_csv('intersections_min.csv')


sensors = sensors_df[["serial", "latitude", "longitude"]].to_numpy()
intersections = intersections_df[["lat", "long"]].to_numpy()

radius = 10
intersections_sensor_relation = []

print("start for intersections_sensor_relation")
for intersection in intersections:
    intersection_lat = intersection[0]
    intersection_long = intersection[1]
    intersection_list = [[intersection_lat, intersection_long], 0, []]
    for sensor in sensors:
        sensor_id = sensor[0]
        sensor_lat = sensor[1]
        sensor_long = sensor[2]

        if sensor_lat < intersection_lat - radius or sensor_lat > intersection_lat + radius:
            continue

        elif sensor_long < intersection_long - radius or sensor_long > intersection_long + radius:
            continue

        else:
            intersection_list[1] += 1
            intersection_list[2].append(
                [sensor_id, sensor_lat, sensor_long])

    intersections_sensor_relation.append(intersection_list)

sensors_in_range = np.array(intersections_sensor_relation)
print("intersections_sensor_relation is done")
sensors_dataset = pd.DataFrame(
    {"Intersection": sensors_in_range[:, 0], "No. Sensors": sensors_in_range[:, 1], "Sensors": sensors_in_range[:, 2]})


file_name = 'senson_intersection_relation_min.csv'
file_path = os.path.join(os.getcwd(), file_name)
if os.path.exists(file_path):
    os.remove(file_path)
# why is co-ordinates of intersection getting saved as string of list?
# change name from temp.csv
sensors_dataset.to_csv('senson_intersection_relation_min.csv', encoding='utf-8')


sensors_dataset = pd.read_csv('senson_intersection_relation_min.csv')

intersection_sensors_detail = sensors_dataset[[
    "Intersection", "No. Sensors", "Sensors"]].to_numpy()

intersection_sensor_distance = []
print("catclulating distance")
for intersection in intersection_sensors_detail:
    if intersection[1] != 0:
        sensors = intersection[2]
        sensors_distance = []

        for sensor in ast.literal_eval(sensors):
            sensor_id = sensor[0]
            sensor_cord = [sensor[1], sensor[2]]
            intersection_cords = ast.literal_eval(intersection[0])
            distance = geodesic(tuple(intersection_cords),
                                tuple(sensor_cord)).meters

            # actual_distance = math.sqrt(distance^2 + 11^2)
            sensors_distance.append([sensor_id, distance])

        intersection_sensor_distance.append(
            [intersection_cords, sensors_distance])

np_intersection_distance = np.array(intersection_sensor_distance)

sensor_distance_dataset = pd.DataFrame(
    {"Intersection": np_intersection_distance[:, 0], "Sensor Distance": np_intersection_distance[:, 1]})
print("distance calculation done")
file_name = 'distance_min.csv'
file_path = os.path.join(os.getcwd(), file_name)
if os.path.exists(file_path):
    os.remove(file_path)
sensor_distance_dataset.to_csv(file_name, encoding='utf-8')

# read distance.csv
# find time taken for each sensors
# store [[{lat,long}, [[sensor_id, time],...]]]..

'''
s = d/t

s = spead of light
d = get from above
t = d/s

s = 3 * 10^8 m/s
d --> meters
'''


sensors_distance_dataset = pd.read_csv('distance_min.csv')

sensor_distance_dataset = sensors_distance_dataset[[
    "Intersection", "Sensor Distance"]].to_numpy()

print("calculating time")
np_sensor_time = []
for intersection in sensor_distance_dataset:
    intersection_cords = intersection[0]
    sensor_distance = intersection[1]
    sensor_time = []
    if len(sensor_distance) != 0:
        for sensor in ast.literal_eval(sensor_distance):
            sensor_id = sensor[0]
            distance = int(sensor[1])
            time = (distance / (3*10**(8)))*(10**9)
            sensor_time.append([sensor_id, time])
        np_sensor_time.append([intersection_cords, sensor_time])

print("calculating toa")
toa = []
for sensor_time in np_sensor_time:
    toa_row = np.zeros(550)
    coords = ast.literal_eval(sensor_time[0])
    toa_row[0] = coords[0]
    toa_row[1] = coords[1]
    if type(sensor_time[1]) != list:
        sensors_list = ast.literal_eval(sensor_time[1])
    else:
        sensors_list = sensor_time[1]
    if len(sensors_list) > 0:
        for sensor in sensors_list:
            sensor_id = sensor[0]
            time_take = sensor[1]
            toa_row[int(sensor_id + 2)] = round(time_take,0)
    toa.append(toa_row)
    
temp = np.array(toa)
df = pd.DataFrame(temp)
df.to_csv("toa_min.csv", encoding='utf-8')       
    









'''
tdoa = []
for intersection_details in np_sensor_time:
    time = [sensor_time[1] for sensor_time in intersection_details[1]]
    sensor_tdoa = []
    min_time = min(time)
    for sensor_detail in intersection_details[1]:
        sensor_id = sensor_detail[0]
        tdoa_value = abs(sensor_detail[1] - min_time)
        sensor_tdoa.append([sensor_id, tdoa_value])
    sensor_tdoa.sort()
    tdoa.append([intersection_details[0], sensor_tdoa])

np_tdoa = np.array(tdoa)

sensor_tdoa_dataset = pd.DataFrame(
    {"Intersection": np_tdoa[:, 0], "Sensor Tdoa": np_tdoa[:, 1]})


file_name = 'tdoa.csv'
file_path = os.path.join(os.getcwd(), file_name)
if os.path.exists(file_path):
    os.remove(file_path)
sensor_tdoa_dataset.to_csv(file_name, encoding='utf-8')




tdoa_np = tdoa_dataset[[
    "Intersection", "Sensor Tdoa"]].to_numpy()


sensor_set = []
coord_set = []

for coord_tdoa in tdoa_np:
    intersection = ast.literal_eval(coord_tdoa[0])
    sensor_tdoa = ast.literal_eval(coord_tdoa[1])
    if len(sensor_tdoa) >= 3:
        sensor_tdoa.sort(key=lambda x: [1])
        closest_sensor = [sensor[0] for sensor in sensor_tdoa[:3]]
        coord_set.append(intersection)
        sensor_set.append(closest_sensor)


df = pd.read_csv('training_1_category_1.csv')
flights = df[[
    "id", "timeAtServer", "aircraft", "latitude", "longitude", "baroAltitude", "geoAltitude", "numMeasurements", "measurements"]].to_numpy()


latlong = []
for flight in flights:
    if flight[2] == 1787:
        sensors = ast.literal_eval(flight[8])
        sensors_list = []
        for sensor in sensors:
            sensor_id = sensor[0]
            sensors_list.append(sensor_id)
        latlong.append(sensors_list)


test_coords = []

for i in range(1, 2):
    neigh = KNeighborsRegressor(n_neighbors=8)
    neigh.fit(sensor_set, coord_set)
    for sensors in latlong:
        if len(sensors) >= 3:
            test_sensors = sensors[:3]
            coords = neigh.predict([test_sensors])
            test_coords.append(coords)

    data = np.array(test_coords)
    x, y = data.T
    plt.scatter(x, y)
    plt.savefig(f"test_graph_2.png")


# 33839,62.0030000209808,1787,49.469970703125,7.93906763980263,6400.8,6537.96,3,"[[424,62969818725,9],[463,62969828597,32],[412,62969874000,10]
'''