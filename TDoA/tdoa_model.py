# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 02:19:37 2020

@author: Asmita
"""
import pdb
import ast
import os

import numpy as np
import pandas as pd
import math


class TDOAModel():
    def __init__(self, toa_dataset):
        self.toa_dataset= toa_dataset
        
    def predict(self, plane_info, temp):
        predicted_values = []
        plane_info_np = np.array(plane_info)
        for row in plane_info_np:
            sensor_ids = [details[0] for details in row]
            total_sensor = len(sensor_ids)
            toa_matches = []
            for toa_details in self.toa_dataset:
                is_sensors = False
                dataset_toa_values = []
                toa_match = {}
                for sensor_id in sensor_ids:
                    if toa_details[sensor_id+2] != 0:
                        is_sensors = True
                        dataset_toa_values.append([sensor_id, 
                                                   toa_details[sensor_id+2]])
                    else:
                        is_sensors = False
                        break
                if is_sensors == True:
                    
                    toa_match["lat"] = toa_details[0]
                    toa_match["long"] = toa_details[1]
                    toa_match["toa"] = {}
                    for data in dataset_toa_values:
                        toa_match["toa"][data[0]] = data[1]
                    toa_matches.append(toa_match)
                    
            if len(toa_matches) > 0:
                row.sort(axis=0)
                plane_toas = {}
                min_toa = row[0][1]
                min_toa_id = row[0][0]
                max_toa = row[0][1]
                max_toa_id = row[0][0]
                for values in row:
                    plane_toas[values[0]] = values[1]
                    if values[1] < min_toa:
                        min_toa = values[1]
                        min_toa_id = values[0]
                    if values[1] >= min_toa:
                        max_toa_id = values[0]
                #min_toa = min(plane_toas)
                rms = 0
                plane_tdoa = {}
                for sensor_id, toa in plane_toas.items():
                    plane_tdoa[sensor_id] = (toa - min_toa)**2
                plane_rms = math.sqrt(sum(list(plane_tdoa.values()))/len(list(plane_tdoa.values())))
                
                tdoa_rmse = []
                for toa_match in toa_matches:
                    if min(toa_match["toa"].values()) != toa_match["toa"][min_toa_id]:
                        continue
                    grid_tdoa = {}
                    grid_tdoa["lat"] = toa_match["lat"]
                    grid_tdoa["long"] = toa_match["long"]
                    grid_tdoa["tdoa"] = {}
                    for sensor_id in toa_match["toa"].keys():
                        grid_tdoa["tdoa"][sensor_id] = toa_match["toa"][sensor_id] - toa_match["toa"][min_toa_id]
                    mean_square = 0
                    for sensor_id in plane_tdoa.keys():
                         mean_square += (grid_tdoa["tdoa"][sensor_id] - plane_tdoa[sensor_id])**2
                    rms = math.sqrt(mean_square/len(list(plane_tdoa.keys())))
                    cord = [grid_tdoa["lat"], grid_tdoa["long"]]
                    tdoa_rmse.append([cord, rms])
                tdoa_rmse.sort(key = lambda x:x[1])
                
                predicted_values.append(tdoa_rmse[0][0])
                
        return predicted_values
        