# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 02:23:11 2020

@author: Asmita
"""

import pdb
import ast
import os

import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from matplotlib import pyplot as plt
from tdoa_model import TDOAModel
from geopy.distance import geodesic
from variation_calc import calc_variation
import datetime
from sklearn.metrics import mean_absolute_error, mean_squared_error

start_time = datetime.datetime.now()
toa = pd.read_csv('toa_min.csv')
toa_np = toa[toa.columns[1:]].to_numpy()


model = TDOAModel(toa_np)


plane_1983 = pd.read_csv("plane_1983.csv")
plane_np = plane_1983[["id", "timeAtServer", "aircraft", "latitude", 
                           "longitude", "baroAltitude", "geoAltitude", 
                           "numMeasurements", "measurements"]].to_numpy()
actual_coord = []
plane_sensor = []
min_sensors = 4
for row in plane_np:
    if row[7] >= min_sensors:
        lat = row[3]
        long = row[4]
        actual_coord.append([lat,long])
        if type(row[8]) != list:
            sensor_details = ast.literal_eval(row[8])
        else:
            sensor_details = row[8]
        sensors_list = [sensor[:2] for sensor in sensor_details]
        plane_sensor.append(sensors_list)
        
values = model.predict(plane_sensor, actual_coord)
print('Mean Absolute Error:', mean_absolute_error(values, actual_coord))
print('Mean Squared Error:', mean_squared_error(values, actual_coord))
print('Root Mean Squared Error:', np.sqrt(
    mean_squared_error(values, actual_coord)))

print('Average distance error: ', calc_variation(values, actual_coord))
end_time = datetime.datetime.now()
print(end_time-start_time)