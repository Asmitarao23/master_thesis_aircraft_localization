import numpy as np
import pandas as pd
import ast

def time_based_split(dataset, num_sensors, test_size):
    flght_routes = dataset[["id", "timeAtServer", "aircraft", "latitude", "longitude",
                            "baroAltitude", "geoAltitude", "numMeasurements", "measurements"]].to_numpy()

    print("line 12")
    for route in flght_routes:
        if int(route[7]) >= 2:
            route[8] = ast.literal_eval(route[8])
            route[8].sort(key=lambda x: x[1])

    sorted_df = flght_routes[np.argsort(flght_routes[0:50000, 1])]
    details = []

    try:
        for each_entry in sorted_df:
            if num_sensors != None:
                if each_entry[7] >= num_sensors and each_entry[3] != None and each_entry[4] != None:
                    flight_location = [each_entry[3], each_entry[4]]
                    sensor_ids = []
                    if type(each_entry[8]) != list:
                        each_entry[8] = ast.literal_eval(each_entry[8])
                    for sensors in each_entry[8][:num_sensors]:
                        flight_location.append(sensors[0])
                    details.extend([flight_location])
            else:
                
                row_details = np.zeros(600)
                if each_entry[3] != None and each_entry[4] != None:
                    row_details[0] = each_entry[3]
                    row_details[1] = each_entry[4]
                    flight_location = [each_entry[3], each_entry[4]]
                    sensor_ids = []
                    if type(each_entry[8]) != list:
                        each_entry[8] = ast.literal_eval(each_entry[8])
                    for sensors in each_entry[8]:
                        sensor_id = int(sensors[0])
                        row_details[sensor_id+2] = 1
                    details.append(row_details)
                
    except Exception as e:
        print(e)
        print("inside ex")
        pdb.set_trace()

    df = pd.DataFrame(details)
    return df
    # x_train = []
    # x_test = []
    # y_train = []
    # y_test = []

    # total_test_data = round(test_size * len(details))

    # for detail in details[:(len(details)-total_test_data)]:
    #     y_train.append(detail[0])
    #     x_train.append(detail[1])

    # for detail in details[(len(details)-total_test_data):]:
    #     y_test.append(detail[0])
    #     x_test.append(detail[1])

    # return x_train, x_test, y_train, y_test
