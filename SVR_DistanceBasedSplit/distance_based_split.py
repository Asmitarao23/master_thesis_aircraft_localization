import numpy as np
import pandas as pd
import ast
import pdb
from math import sin, cos, sqrt, atan2, radians


def calc_variation(point_1, point_2):
    distance = []
    R = 6373.0
    lat1 = radians(point_1[0])
    lon1 = radians(point_1[1])
    lat2 = radians(point_2[0])
    lon2 = radians(point_2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = R * c
    return distance


def distance_based_split(dataset, num_sensors, test_size, sensor_file="sensors.csv"):
    flght_routes = dataset[["id", "timeAtServer", "aircraft", "latitude", "longitude",
                            "baroAltitude", "geoAltitude", "numMeasurements", "measurements"]].to_numpy()
    sensors_df = pd.read_csv(sensor_file)
    sensors = sensors_df[["serial", "latitude", "longitude"]].to_numpy()

    details = []
    print("line 12")
    for route in flght_routes:
        if int(route[7]) >= 2:
            route[8] = ast.literal_eval(route[8])
            for sensor in route[8]:
                sensor_id = sensor[0]
                plane_location = [route[3], route[4]]
                sensor_details = sensors[np.where(sensors[:, 0] == sensor_id)]
                sensor_location = [sensor_details[0][1], sensor_details[0][2]]
                distance = calc_variation(plane_location, sensor_location)
                sensor.append(distance)
            route[8].sort(key=lambda x: x[3])
    print("done sorting by distance")

    sorted_df = flght_routes[np.argsort(flght_routes[0:200000, 1])]
    print("done sorting by server time")
    try:
        for each_entry in sorted_df:
            if each_entry[7] >= num_sensors and each_entry[3] != None and each_entry[4] != None:
                flight_location = [each_entry[3], each_entry[4]]
                sensor_ids = []
                if type(each_entry[8]) != list:
                    each_entry[8] = ast.literal_eval(each_entry[8])
                for sensors in each_entry[8][:num_sensors]:
                    flight_location.append(sensors[0])
                details.extend([flight_location])
    except Exception as e:
        print(e)
        print("inside ex")
        print("got all details")
        
    df = pd.DataFrame(details)
    return df
