# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 01:37:59 2020

@author: Asmita
"""

import pdb
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVR
from sklearn.metrics import mean_absolute_error, mean_squared_error
from matplotlib import pyplot as plt
from sklearn.multioutput import MultiOutputRegressor
from sklearn.model_selection import KFold
from variation_calc import calc_variation
from time_based_split import time_based_split
from distance_based_split import distance_based_split
import folium
from sklearn import metrics
import datetime
from get_data import get_data

#dataframe = pd.read_csv("training_1_category_1.csv")
dataframe = pd.read_csv("SampleFile.csv")

# # using time

df = time_based_split(dataframe, num_sensors=4, test_size=0.3)
df = df.dropna(axis=0, how='any')

X = df.iloc[:, 2:6].values
y = df.iloc[:, 0:2].values



# # sc_X = StandardScaler()
# # sc_y = StandardScaler()
# # X = sc_X.fit_transform(X)
# # y = sc_y.fit_transform(y)

x_train, x_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=0)

# scaling = MinMaxScaler(feature_range=(-1, 1)).fit(x_train)
# x_train = scaling.transform(x_train)
# x_test = scaling.transform(x_test)

regressor = SVR(kernel='rbf')

multioutputregressor = MultiOutputRegressor(
    SVR(kernel='rbf')).fit(x_train, y_train)

multioutputregressor.score(x_test, y_test)

y_pred = multioutputregressor.predict(x_test)

print(metrics.mean_squared_error(y_test, y_pred))
print(metrics.r2_score(y_test, y_pred))
print('Mean Absolute Error:', mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(
    mean_squared_error(y_test, y_pred)))

print('Average distance error: ', calc_variation(y_test, y_pred))

dataframe_1787 = pd.read_csv("plane_1983.csv")



df_1787 = time_based_split(dataframe_1787, num_sensors= 4, test_size=0.3)
df_1787 = df_1787.dropna(axis=0, how='any')
X_1787 = df_1787.iloc[:, 2:6].values
y_1787 = df_1787.iloc[:, 0:2].values

y_pred_1787 = multioutputregressor.predict(X_1787)
print('Average distance error: ', calc_variation(y_1787, y_pred_1787, plane=True))

'''
m = folium.Map(location=[49.5227361129502, 7.80571190086571])
folium.PolyLine(y_1787, color="red", weight=2.5, opacity=1).add_to(m)
[folium.CircleMarker(row, radius=1, popup="", color='green', fill_color="red").add_to(m) for row in y_pred_1787]
m.save("svr_plot_2.html") 21m 39s

'''
