# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 18:20:21 2020

@author: Nikhil
"""

import numpy as np
import pandas as pd
import ast


def get_data(dataset):
    flght_routes = dataset[["id", "aircraft", "latitude", "longitude", "numMeasurements",
                            "S1","S2","S3","S4","S5","S6","S7","S8"]].to_numpy()

    print("line 12")
    
    for route in flght_routes:
        if int(route[7]) >= 2:
            route[8] = ast.literal_eval(route[8])
            route[8].sort(key=lambda x: x[1])

    sorted_df = flght_routes[np.argsort(flght_routes[:, 1])]
    details = []
    for each_entry in sorted_df[0:200000]:
        row_details = np.zeros(600)
        if each_entry[3] != None and each_entry[4] != None and each_entry[2] == 1983:
            row_details[0] = each_entry[3]
            row_details[1] = each_entry[4]
            flight_location = [each_entry[3], each_entry[4]]
            sensor_ids = []
            if type(each_entry[8]) != list:
                each_entry[8] = ast.literal_eval(each_entry[8])
            for sensors in each_entry[8]:
                sensor_id = int(sensors[0])
                row_details[sensor_id+2] = 1
            details.append(row_details)
    df = pd.DataFrame(details)
    return df