import pdb
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
from sklearn.metrics import mean_absolute_error, mean_squared_error
from matplotlib import pyplot as plt
from time_based_split import time_based_split
from distance_based_split import distance_based_split
from variation_calc import calc_variation
from time_based_split import time_based_split
from distance_based_split import distance_based_split
import folium
from sklearn import metrics
import datetime
from matplotlib import pyplot as plt



import datetime

start_time = datetime.datetime.now()

# df = pd.read_csv("flight_details_4.csv")

# df = df.dropna(axis=0, subset=["latitude",
#                                "longitude", "S1", "S2", "S3", "S4", "S5"])

#dataframe = pd.read_csv("training_1_category_1.csv")
dataframe = pd.read_csv("flight_details_4.csv")
df = dataframe.dropna(axis=0, subset=["latitude",
                    "longitude", "S1", "S2", "S3", "S4"])
X = df.iloc[:, 5:9].values
y = df.iloc[:, 2:4].values

# # using time
'''
df = time_based_split(dataframe, 4, test_size=0.3)
df = df.dropna(axis=0, how='any')

X = df.iloc[:, 2:6].values
y = df.iloc[:, 0:2].values
'''

'''
# # using distance
df = distance_based_split(dataframe, 4, test_size=0.3)
df = df.dropna(axis=0, how='any')

X = df.iloc[:, 2:6].values
y = df.iloc[:, 0:2].values
'''



'''

x   y    s1   s2   s3   s400     ..... s536
4,  7    0    0    0     1

'''

'''
x_train, x_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=0)

pdb.set_trace()

'''
regressor = KNeighborsRegressor(n_neighbors=20, weights='distance')

'''
regressor.fit(x_train, y_train)

y_pred = regressor.predict(x_test)

'''

cv = KFold(n_splits=10)

for train_index, test_index in cv.split(X):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # For training, fit() is used
    regressor.fit(X_train, y_train)

    # Default metric is R2 for regression, which can be accessed by score()
    regressor.score(X_test, y_test)

    # For other metrics, we need the predictions of the model
    y_pred = regressor.predict(X_test)

    print(metrics.mean_squared_error(y_test, y_pred))
    print(metrics.r2_score(y_test, y_pred))
    
print('Mean Absolute Error:', mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(
    mean_squared_error(y_test, y_pred)))

print('Average distance error: ', calc_variation(y_test, y_pred))

dataframe_1787 = pd.read_csv("plane_1983.csv")   
#dataframe_1787 = pd.read_csv("1984_onlysensors.csv") 
#dataframe_1787 = pd.read_csv("1984_onlysensors.csv")
df_1787 = time_based_split(dataframe_1787, num_sensors= 4, test_size=0.3)
# df_1787 = df_1787.dropna(axis=0, how='any')
X_1787 = df_1787.iloc[:, 2:6].values
y_1787 = df_1787.iloc[:, 0:2].values

y_pred_1787 = regressor.predict(X_1787)
print('Average distance error: ', calc_variation(y_1787, y_pred_1787))

'''
m = folium.Map(location=[49.5227361129502, 7.80571190086571])
folium.PolyLine(y_1787, color="red", weight=2.5, opacity=1).add_to(m)
folium.PolyLine(y_pred_1787, color="green", weight=2.5, opacity=1).add_to(m)
m.save("rf_kfold_1.html")

'''
end_time = datetime.datetime.now()
print(end_time-start_time)
# data = np.array(y_pred)
# x, y = data.T
# plt.scatter(x, y)
# plt.savefig(f"rf_pred_4.png")

# data = np.array(y_test)
# x, y = data.T
# plt.scatter(x, y)
# plt.savefig(f"rf_test_4.png")

# folium library
